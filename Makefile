.PHONY: help serve cleanup

# HELP
# This will output the help for each task
# thanks to https://marmelab.com/blog/2016/02/29/auto-documented-makefile.html

# target names are selected based on https://gazr.io
help: ## This help.
	@awk 'BEGIN {FS = ":.*?## "} /^[a-z%A-Z_-]+:.*?## / {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}' $(MAKEFILE_LIST)

upgrade-theme: ## upgrade theme
	pushd ./themes/hugo-notepadium ;\
	git pull ;\
	popd

serve-all: ## run local hugo server, build also drafts
	/usr/local/bin/hugo server --buildDrafts --buildExpired --buildFuture --enableGitInfo --ignoreCache --disableFastRender

cleanup: ## run garbage collector
	/usr/local/bin/hugo --gc
