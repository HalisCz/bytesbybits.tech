---
title: "How to remove GPS data from picture's metadata"
date: 2020-04-22T22:31:11+02:00
draft: true
categories: ["posts"]
tags: ["exif", "git", "gps", "privacy"]
---

Most of the cameras used today are enclosing lots of (so called [EXIF](https://en.wikipedia.org/wiki/Exif)) metadata to the picture. One of that metadata is GPS location, which sometimes may be shared unwillingly.

To prevent accidental sharing of the location where the picture was created, you can use this script to strip away GPS location from rest of the EXIF data.

Script is using [exiftool](https://exiftool.org/), and it can be run as [git pre-commit hook](https://git-scm.com/book/en/v2/Customizing-Git-Git-Hooks).
