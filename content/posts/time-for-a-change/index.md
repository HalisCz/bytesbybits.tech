---
title: "Time for a Change"
date: 2020-12-04T19:31:34+01:00
categories: ["linux2mac"]
tags: ["mac"]
---

**I've been using GNU/Linux as my main OS since 2008. During that time, I've slowly transitioned across a few phases such as "getting to know what OS is" (thanks Ubuntu), "getting to know what my OS consists of" (thanks Gentoo), and "I like GNU/Linux, but I don't have time to micromanage everything (thanks Fedora)".**

Not only that I shifted across various Linux distributions, but for the last year and a half, my day-to-day work shifted too. From SSH and console to browser, from Ansible and Bash to MS Office, and from personal communication with the team and a few emails to video calls (looking angrily at COVID-19) and an awfully lot of emails. Because of that, I was slowly driven mad by using in-browser MS Office and unreliable Linux versions of videoconferencing tools in combination with a poorly working implementation of a mixed-DPI setup.

These issues may seem futile, but when those activities become more frequent, the figurative pain in the neck became just too much. Just to illustrate, when my colleague left some excel spreadsheet scaled to 300% view, there is no way how to fix this using the in-browser app. And although screen-sharing and multi-monitor setup may seem like two distinguished issues, I could make a choice during every reboot - either use Wayland with mixed-DPI support for far-from-flawless multi-monitor setup without screen-sharing support, or vice versa Xorg server with screen-sharing support without mixed-DPI multi-monitor support.

To avoid this madness, I decided to try a different, more mainstream OS, which leaves MS Windows and Mac OS on the shortlist. And since I was not ready to give up on the command line and software in repositories, MS Windows had to settle for second place in my personal race.

OS was chosen, and then there was the hardware selection. Because of portability reasons, I have eliminated iMacs and MacBook Pro 16". For performance reasons, I've excluded MacBook Air. Therefore, the chosen one was MacBook 13". The last decision remaining (in 2020) is something new - which architecture? Intel (x86) or Apple Silicon (ARM)? Because of compatibility concerns, I cowardly stuck to Intel.

Naturally, I was worried about switching OS after a decade, so I was not willing to bet that amount of money on some kind of experiment. Luckily, there is an option of "operating lease" called Alza Neo in my country, which makes this hardware available for a monthly price equivalent to a full tank of gas.

That being said, I just picked up my new MacBook :)